/*!
 * date.js JavaScript Library
 *
 * Copyright (c) 2019-2021 Milan Bartky
 * Released under the MIT license
 *
 * First Release: 2019-01-16T17:31Z
 */
(function() {
    /*
     * Helper functions
     */
    function nth(d) {
        if (d > 3 && d < 21) return 'th';
        switch (d % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }
    function getDayofYear(d) {
        var timestmp = new Date().setFullYear(d.getFullYear(), 0, 1);
        var yearFirstDay = Math.floor(timestmp / 86400000);
        var today = Math.floor((d.getTime()) / 86400000);
        return today - yearFirstDay;
    }
    function getWeekNumber(d) {
        // Copy date so don't modify original
        d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
        // Get first day of year
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
        // Calculate full weeks to nearest Thursday
        var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
        // Return array of year and week number
        return weekNo;
    }
    function leapYear(year) {
        if (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0))
            return 1;
        return 0;
    }
    function getAmPm(d) {
        if (d.getHours() + 1 > 12) {
            return "pm";
        }
        return "am";
    }
    function swatch(d) {
        var h = d.getHours();
        var m = d.getMinutes();
        var s = d.getSeconds();
        var tzoff = 60 + d.getTimezoneOffset();
        return ('000' + Math.floor((h * 3600 + (m + tzoff) * 60 + s) / 86.4) % 1000).slice(-3);
    }
    function twelveHours(d) {
        var hours = d.getHours();
        if (hours > 12) {
            hours -= 12;
        } else if (hours === 0) {
            hours = 12;
        }
        return hours;
    }
    function daylightsaving(d) {
        var jan = new Date(d.getFullYear(), 0, 1);
        var jul = new Date(d.getFullYear(), 6, 1);
        var x = Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());

        if (d.getTimezoneOffset() < x)
            return 1;
        return 0;
    }
    function getTimezoneOffset(d) {
        function z(n) {
            return (n < 10 ? '0' : '') + n
        }
        var offset = d.getTimezoneOffset();
        var sign = offset < 0 ? '+' : '-';
        offset = Math.abs(offset);
        return sign + z(offset / 60 | 0) + z(offset % 60);
    }




    /*
     * Main date function
     */
    const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
          months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    function _date(timestamp) {
        this._formatTable = {};

        this._timestamp = timestamp;
        if (isNaN(this._timestamp)) {
            this._timestamp = Date.now();
        }
        this.date = new Date(this._timestamp);
        
        this._formatFuncs = {
            d: () => ("0" + this.date.getDate()).slice(-2),
            D: () => days[this.date.getDay()].slice(0, 3),
            j: () => "" + this.date.getDate(),
            l: () => days[this.date.getDay()],
            N: () => ((this.date.getDay() === 0) ? "7" : this.date.getDay() + ""),
            S: () => nth(this.date.getDate()),
            w: () => this.date.getDay() + "",
            z: () => getDayofYear(this.date) + "",
            W: () => ("0" + getWeekNumber(this.date)).slice(-2),
            F: () => months[this.date.getMonth()],
            m: () => ("0" + (this.date.getMonth() + 1)).slice(-2),
            M: () => months[this.date.getMonth()].slice(0, 3),
            n: () => (this.date.getMonth() + 1) + "",
            t: () => (new Date(this.date.getFullYear(), this.date.getMonth()+1, 0)).getDate(),
            L: () => leapYear(this.date.getFullYear()) + "",
            o: () => this.date.toISOString().slice(0, 4),
            Y: () => this.date.getFullYear() + "",
            y: () => (this.date.getYear() + "").slice(-2),
            a: () => getAmPm(this.date),
            A: () => getAmPm(this.date).toUpperCase(),
            B: () => swatch(this.date),
            g: () => twelveHours(this.date) + "",
            G: () => this.date.getHours() + "",
            h: () => ("0" + twelveHours(this.date)).slice(-2),
            H: () => ("0" + this.date.getHours()).slice(-2),
            i: () => ("0" + this.date.getMinutes()).slice(-2),
            s: () => ("0" + this.date.getSeconds()).slice(-2),
            u: date.IS_NOT_SUPPORTED,
            v: () => ("000" + this.date.getMilliseconds()).slice(-3),
            e: date.IS_NOT_SUPPORTED,
            I: () => daylightsaving(this.date) + "",
            O: () => getTimezoneOffset(this.date),
            P: () => getTimezoneOffset(this.date).slice(0, 3) + ":" + getTimezoneOffset(this.date).slice(-2),
            T: date.IS_NOT_SUPPORTED,
            Z: date.IS_NOT_SUPPORTED,
            c: () => this.date.getFullYear() + "-" + ("0" + (this.date.getMonth() + 1)).slice(-2) + "-" + ("0" + this.date.getDate()).slice(-2) + "T" + ("0" + this.date.getHours()).slice(-2) + ":" + ("0" + this.date.getMinutes()).slice(-2) + ":" + ("0" + this.date.getSeconds()).slice(-2) + getTimezoneOffset(this.date).slice(0, 3) + ":" + getTimezoneOffset(this.date).slice(-2),
            r: date.IS_NOT_SUPPORTED,
            U: () => Math.round(this.date.getTime() / 1000) + ""
        };
    }

    window.date = function(timestamp) {
        return new _date(timestamp);
    };
    window.date.IS_NOT_SUPPORTED = 'NOT_SUPPORTED';

    _date.prototype.format = function(format) {
        const characters = format.split('');

        let result = '';

        for (let x=0;x<characters.length;x++) {
            const character = characters[x];

            result += this._get(character);
        }

        return result;
    };

    _date.prototype._get = function(char) {
        if (char in this._formatFuncs) {
            if (this._formatFuncs[char] === date.IS_NOT_SUPPORTED) {
                return date.IS_NOT_SUPPORTED;
            } else if (!(char in this._formatTable)) {
                this._formatTable[char] = this._formatFuncs[char]();
            }
            return this._formatTable[char];
        }
        return char;
    };

    const testFormat = 'd - D - j - l - N - S - w - z - W - F - m - M - n - t - L - o - Y - y - a - A - B - g - G - h - H - i - s - u - v - e - I - O - P - p - T - Z - c - r - U'.split(' - '),
          testCompare = '27 - Wed - 27 - Wednesday - 3 - th - 3 - 177 - 26 - June - 06 - Jun - 6 - 30 - 0 - 2018 - 2018 - 18 - pm - PM - 778 - 7 - 19 - 07 - 19 - 41 - 27 - 000000 - 000 - Europe/Berlin - 1 - +0200 - +02:00 - p - CEST - 7200 - 2018-06-27T19:41:27+02:00 - Wed, 27 Jun 2018 19:41:27 +0200 - 1530121287'.split(' - '),
          testTimestamp = 1530121287 * 1000;

    window.testDate = function() {
        for (let x=0;x<testFormat.length;x++) {
            const format = testFormat[x];
            const compare = testCompare[x];

            const result = date(testTimestamp).format(format);

            if (result !== compare && result !== date.IS_NOT_SUPPORTED) {
                console.log("--- Fail\nFormat:   " + format + "\nExpected: " + compare + "\nReturned: " + result)
            }
        }
    }
})();
