var fs = require('fs');

process.env.TZ = 'Europe/Berlin';

var contents = fs.readFileSync('../date.js', 'utf8');
eval(contents);

var time = 1547662284475;

var format = "d D j l N S w z W F m M n t L o Y y a A B g G h H i s v I O P c U";

var characters = format.split(" ");

for (var x=0;x<characters.length;x++) {
    var str = date(characters[x], time);
    var valid = "yes";
    if (!isValid(characters[x], str)) {
        valid = "no";
    }
    
    console.log(characters[x] + ":\n    expected: " + getExpected(characters[x]) + "\n    result  : " + str + "\n    valid   : " + valid);
    
    if (valid === "no") {
        process.exit(22);
    }
}

function getExpected(format) {
    switch(format) {
        case "d":
            return "16";
        case "D":
            return "Wed";
        case "j":
            return "16";
        case "l":
            return "Wednesday";
        case "N":
            return "3";
        case "S":
            return "th";
        case "w":
            return "3";
        case "z":
            return "16";
        case "W":
            return "03";
        case "F":
            return "January";
        case "m":
            return "01";
        case "M":
            return "Jan";
        case "n":
            return "1";
        case "t":
            return "31";
        case "L":
            return "0";
        case "o":
            return "2019";
        case "Y":
            return "2019";
        case "y":
            return "19";
        case "a":
            return "pm";
        case "A":
            return "PM";
        case "B":
            return "799";
        case "g":
            return "7";
        case "G":
            return "19";
        case "h":
            return "07";
        case "H":
            return "19";
        case "i":
            return "11";
        case "s":
            return "24";
        case "v":
            return "475";
        case "I":
            return "0";
        case "O":
            return "+0100";
        case "P":
            return "+01:00";
        case "c":
            return "2019-01-16T19:11:24+01:00";
        case "U":
            return "1547662284";
        default:
            return "";
    }
}
function isValid(format, string) {
    switch(format) {
        case "d":
            return string === "16";
        case "D":
            return string === "Wed";
        case "j":
            return string === "16";
        case "l":
            return string === "Wednesday";
        case "N":
            return string === "3";
        case "S":
            return string === "th";
        case "w":
            return string === "3";
        case "z":
            return string === "16";
        case "W":
            return string === "03";
        case "F":
            return string === "January";
        case "m":
            return string === "01";
        case "M":
            return string === "Jan";
        case "n":
            return string === "1";
        case "t":
            return string === "31";
        case "L":
            return string === "0";
        case "o":
            return string === "2019";
        case "Y":
            return string === "2019";
        case "y":
            return string === "19";
        case "a":
            return string === "pm";
        case "A":
            return string === "PM";
        case "B":
            return string === "799";
        case "g":
            return string === "7";
        case "G":
            return string === "19";
        case "h":
            return string === "07";
        case "H":
            return string === "19";
        case "i":
            return string === "11";
        case "s":
            return string === "24";
        case "v":
            return string === "475";
        case "I":
            return string === "0";
        case "O":
            return string === "+0100";
        case "P":
            return string === "+01:00";
        case "c":
            return string.startsWith("2019-01-16T19:11:24");
        case "U":
            return string === "1547662284";
        default:
            return true;
    }
}